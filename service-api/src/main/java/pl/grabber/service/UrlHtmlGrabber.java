package pl.grabber.service;


import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.DomSerializer;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.TagNode;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import pl.grabber.model.config.AdvertisementConfig;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.net.URL;

public class UrlHtmlGrabber {

    AdvertisementConfig advertisementConfig;

    public UrlHtmlGrabber(AdvertisementConfig advertisementConfig) {
        this.advertisementConfig = advertisementConfig;

    }

    public Document getDocument(String urlString){
        Document doc = null;
        try {
            doc = Jsoup.connect(urlString).get();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Elements newsHeadlines = doc.select("#mp-itn b a");
        return doc;
    }

    public void getAdvertisements(Document doc){
        doc.select(advertisementConfig.)
    }

}
