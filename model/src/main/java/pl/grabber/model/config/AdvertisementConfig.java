package pl.grabber.model.config;


public class AdvertisementConfig {

    private String url;
    private String listAdvSelector;
    private String titleSelector;
    private String descSelector;
    private String imagesSelector;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getListAdvSelector() {
        return listAdvSelector;
    }

    public void setListAdvSelector(String listAdvSelector) {
        this.listAdvSelector = listAdvSelector;
    }

    public String getTitleSelector() {
        return titleSelector;
    }

    public void setTitleSelector(String titleSelector) {
        this.titleSelector = titleSelector;
    }

    public String getDescSelector() {
        return descSelector;
    }

    public void setDescSelector(String descSelector) {
        this.descSelector = descSelector;
    }

    public String getImagesSelector() {
        return imagesSelector;
    }

    public void setImagesSelector(String imagesSelector) {
        this.imagesSelector = imagesSelector;
    }
}
